import { useStorage } from "@vueuse/core";

// 定义了一个全局响应式变量count
export const count = useStorage("count", 0);
