import axios from 'axios';
import type { RouteRecordNormalized } from 'vue-router';
import { UserState } from '@/store/modules/user/types';

// 和我们定义golang strcut
export interface LoginData {
  user_name: string;
  password: string;
}

// js 版本
// export function login(data) {
//   return axios.post('/api/user/login', data);
// }

// ts interface 有点像struct
export interface LoginRes {
  access_token: string;
  issue_at: number;
  access_token_expired_at: number;
  refresh_token: number;
  refresh_token_expired_at: number;
}

// data: LoginData 表示你要传递的data object必须满足LoginData接口约束
// {user_name: "admin", "password": "12345"}
// 怎么约束返回值的类型, return {}, <类型>
// 返回必须是一个LoginRes对象, {token: "xxxx"}
// 前端设置代理
// post<T = any, R = AxiosResponse<T>, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>): Promise<R>;
// post(url, data, config);
export function login(data: LoginData) {
  return axios.post<LoginRes>('/keyauth-g7/api/v1/token/issue', data);
}

export function logout() {
  return axios.post<LoginRes>('/api/user/logout');
}

export function getUserInfo() {
  return axios.post<UserState>('/api/user/info');
}

export function getMenuList() {
  return axios.post<RouteRecordNormalized[]>('/api/user/menu');
}
