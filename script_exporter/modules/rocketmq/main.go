package main

import (
	"fmt"
	"os"

	"gitee.com/go-course/go7/script_exporter/modules/rocketmq/collector"
	"gitee.com/go-course/go7/script_exporter/modules/rocketmq/conf"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

func main() {
	// 读取配置
	if err := conf.LoadConfigFromEnv(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// 采用新的注册表
	retgistry := prometheus.NewRegistry()

	// 如果打印采集后的结果
	c := collector.NewCollector()

	// 把写好的采集器注册到当前的注册表
	if err := retgistry.Register(c); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// 通过Gather方法触发采集
	mf, err := retgistry.Gather()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// 如何打印我们mf
	// 编码后直接输出到标准输出
	enc := expfmt.NewEncoder(os.Stdout, expfmt.FmtText)
	for i := range mf {
		enc.Encode(mf[i])
	}
}
