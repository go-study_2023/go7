package collector

import (
	"encoding/json"
	"strconv"
)

type RocketMQMetric struct {
	Group     string
	Count     string
	Version   string
	Type      string
	Model     string
	TPS       string
	DiffTotal string
}

func (m *RocketMQMetric) Float64Count() float64 {
	c, err := strconv.ParseFloat(m.Count, 64)
	if err != nil {
		panic(err)
	}
	return c
}
func (m *RocketMQMetric) Float64Tps() float64 {
	c, err := strconv.ParseFloat(m.TPS, 64)
	if err != nil {
		panic(err)
	}
	return c
}
func (m *RocketMQMetric) Float64DiffTotal() float64 {
	c, err := strconv.ParseFloat(m.DiffTotal, 64)
	if err != nil {
		panic(err)
	}
	return c
}
func (m *RocketMQMetric) String() string {
	dj, _ := json.Marshal(m)
	return string(dj)
}

type RocketMQMetricSet struct {
	Items []*RocketMQMetric
}

func (s *RocketMQMetricSet) Add(item *RocketMQMetric) {
	s.Items = append(s.Items, item)
}

func ParseLine(line string) *RocketMQMetric {
	words := []string{}
	chars := []rune{}
	for _, c := range line {
		if c == ' ' {
			// 组装成单词 转移到words里面
			if len(chars) > 0 {
				words = append(words, string(chars))
				chars = []rune{}
			}
		} else {
			chars = append(chars, c)
		}
	}

	if len(chars) > 0 {
		words = append(words, string(chars))
	}

	// words --> metric
	// Count  #Version                 #Type  #Model          #TPS     #Diff Total
	return &RocketMQMetric{
		Group:     words[0],
		Count:     words[1],
		Version:   words[2],
		Type:      words[3],
		Model:     words[4],
		TPS:       words[5],
		DiffTotal: words[6],
	}
}
