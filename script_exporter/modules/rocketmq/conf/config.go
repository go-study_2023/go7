package conf

func NewDefaultConfig() *Config {
	return &Config{
		FileConfig: &fileConfig{
			Path: "modules/rocketmq/collector/sample/data.txt",
		},
	}
}

type Config struct {
	FileConfig *fileConfig `toml:"file"`
}

type fileConfig struct {
	Path string `toml:"path" env:"DATA_FILE_PATH"`
}
