package controller_test

import (
	"bytes"
	"testing"

	"gitee.com/go-course/go7/script_exporter/controller"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	sc *controller.ScriptCollector
)

func TestScriptExec(t *testing.T) {
	b := bytes.NewBuffer([]byte{})
	// e:\Projects\Golang\go-course-projects\go7\go7\script_exporter\modules\rocketmq_collector.exe
	// e:\Projects\Golang\go-course-projects\go7\go7\script_exporter\modules\test.py
	if err := sc.Exec("rocketmq_exporter.exe", "localhost", b); err != nil {
		t.Fatal(err)
	}

	t.Log(b.String())
}

func init() {
	zap.DevelopmentSetup()

	sc = controller.NewScriptCollector("../modules")
}
