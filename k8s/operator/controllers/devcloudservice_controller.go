/*
Copyright 2022 Lao Yu.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	devcloudv1 "gitee.com/go-course/go7/k8s/operator/api/v1"
)

// DevcloudServiceReconciler reconciles a DevcloudService object
type DevcloudServiceReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=devcloud.go7,resources=devcloudservices,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=devcloud.go7,resources=devcloudservices/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=devcloud.go7,resources=devcloudservices/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the DevcloudService object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.0/pkg/reconcile
func (r *DevcloudServiceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	// 获取日志对象
	l := log.FromContext(ctx, "namespace", req.Namespace)

	// TODO(user): your logic here
	// 1.通过名称获取DevcloudService对象, 并打印
	l.Info("start get API Object: DevcloudService")
	object := &devcloudv1.DevcloudService{}
	if err := r.Get(ctx, req.NamespacedName, object); err != nil {
		// 如果Not Found则表示该资源已经删除, 需要做删除处理
		if apierrors.IsNotFound(err) {
			l.Info("delete API Object: DevcloudService", "namespace", req.Namespace, "name", req.Name)
			return ctrl.Result{}, nil
		}
		l.Error(err, "get object error")
		return ctrl.Result{}, err
	}

	l.Info("get DevcloudService object",
		"name", object.Name,
		"url", object.Spec.URL)

	// 判断对象的状态, 是否需要处理
	if object.Status.IsSync {
		l.Info("object has deal", "name", req.Name)
		return ctrl.Result{}, nil
	}

	// 需要处理该对象, 处理完成后就需要变更对象的状态
	// 把该Ojbect 映射其他系统, 比如Traefik
	l.Info("sync to traefik, registry service to traefik",
		"name", object.Name,
		"url", object.Spec.URL,
	)
	object.Status.IsSync = true
	// 更新对象
	if err := r.Status().Update(ctx, object); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DevcloudServiceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&devcloudv1.DevcloudService{}).
		Complete(r)
}
